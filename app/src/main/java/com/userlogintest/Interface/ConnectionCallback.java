package com.userlogintest.Interface;

public interface ConnectionCallback {

    public enum ConnResult
    {
        SUCCESS,
        FAILED
    }

    public interface GetObjectCallBack
    {
        public void connectionDidFinished(ConnResult result,String responseMessage,Object object);
    }

}
