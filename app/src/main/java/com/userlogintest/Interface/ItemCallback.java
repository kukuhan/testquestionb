package com.userlogintest.Interface;

import com.userlogintest.Model.ItemModel;

public interface ItemCallback {

    void onItemLongclick(ItemModel mItem);

    void onItemClick(ItemModel mItem);
}
