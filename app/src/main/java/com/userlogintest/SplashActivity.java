package com.userlogintest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.userlogintest.Utils.LogHelper;
import com.userlogintest.Utils.SharedPrefManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        finishSplash();
    }

    private void finishSplash(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                LogHelper.debug("finish Splash");
                goToMainPage();
            }
        }, 2000);
    }

    private void goToMainPage(){
        Intent i;
        if(SharedPrefManager.getInstance(this).getBoolean(SharedPrefManager.TAG_IS_LOGIN, false)){
            i = new Intent(this, MainActivity.class);
        }else{
            i = new Intent(this, LoginActivity.class);
        }
        this.startActivity(i);
        this.finish();
    }
}
