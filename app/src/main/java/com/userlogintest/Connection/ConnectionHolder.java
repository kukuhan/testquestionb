package com.userlogintest.Connection;

import android.content.Context;

import com.userlogintest.Interface.ConnectionCallback;
import com.userlogintest.Model.ItemModel;
import com.userlogintest.Model.StatusModel;
import com.userlogintest.Utils.CustomRetrofit;
import com.userlogintest.Utils.JSonHelper;
import com.userlogintest.Utils.LogHelper;
import com.userlogintest.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectionHolder {

    public static void getListing(final Context c, final ConnectionCallback.GetObjectCallBack callBack){
        Map<String, String> map = new HashMap<>();
        map.put("id", SharedPrefManager.getInstance(c).getString(SharedPrefManager.TAG_USER_ID));
        map.put("token", SharedPrefManager.getInstance(c).getString(SharedPrefManager.TAG_USER_TOKEN));
        CustomRetrofit.getInstance().listing(map).enqueue(new Callback<ResponseBody>() {
            List<ItemModel> list = new ArrayList();
            ConnectionCallback.ConnResult result = ConnectionCallback.ConnResult.FAILED;
            String errorMsg;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String responseBody = response.body().string();

                    LogHelper.debug("[responseBody] " + responseBody);
                    StatusModel status = JSonHelper.parseStatus(responseBody);
                    errorMsg = status.getMessage();

                    if (status.getCode() == StatusModel.SUCCESS_CODE) {
                        list = JSonHelper.parseItem(responseBody);
                        result = ConnectionCallback.ConnResult.SUCCESS;
                    }

                }catch (Exception e){

                }
                callBack.connectionDidFinished(result, errorMsg, list);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String errorMsg = getRetrofitErrorMsg(t);
                callBack.connectionDidFinished(result, errorMsg, null);
            }
        });

    }

    public static void updateItem(final Context c, ItemModel _mItem, final ConnectionCallback.GetObjectCallBack callBack){
        Map<String, String> map = new HashMap<>();
        map.put("id", SharedPrefManager.getInstance(c).getString(SharedPrefManager.TAG_USER_ID));
        map.put("token", SharedPrefManager.getInstance(c).getString(SharedPrefManager.TAG_USER_TOKEN));
        map.put("listing_id", _mItem.getId());
        map.put("listing_name", _mItem.getName());
        map.put("distance", _mItem.getDistance());
        CustomRetrofit.getInstance().update(map).enqueue(new Callback<ResponseBody>() {
            ConnectionCallback.ConnResult result = ConnectionCallback.ConnResult.FAILED;
            String errorMsg;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String responseBody = response.body().string();

                    LogHelper.debug("[responseBody] " + responseBody);
                    StatusModel status = JSonHelper.parseStatus(responseBody);
                    errorMsg = status.getMessage();

                    if (status.getCode() == StatusModel.SUCCESS_CODE) {
                        result = ConnectionCallback.ConnResult.SUCCESS;
                    }

                }catch (Exception e){

                }
                callBack.connectionDidFinished(result, errorMsg, null);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String errorMsg = getRetrofitErrorMsg(t);
                callBack.connectionDidFinished(result, errorMsg, null);
            }
        });

    }

    public static void signInUser(final Context c, Map<String, String> map, final ConnectionCallback.GetObjectCallBack callBack){
        CustomRetrofit.getInstance().login(map).enqueue(new Callback<ResponseBody>() {
            ConnectionCallback.ConnResult result = ConnectionCallback.ConnResult.FAILED;
            String errorMsg;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String responseBody = response.body().string();

                    LogHelper.debug("[responseBody] " + responseBody);
                    StatusModel status = JSonHelper.parseStatus(responseBody);
                    errorMsg = status.getMessage();

                    if (status.getCode() == StatusModel.SUCCESS_CODE) {
                        Map<String, String> map = JSonHelper.parseUserLogin(responseBody);
                        SharedPrefManager.getInstance(c).putString(SharedPrefManager.TAG_USER_ID, map.get("id"));
                        SharedPrefManager.getInstance(c).putString(SharedPrefManager.TAG_USER_TOKEN, map.get("token"));
                        SharedPrefManager.getInstance(c).putBoolean(SharedPrefManager.TAG_IS_LOGIN, true);
                        LogHelper.info("[signInUser] " + responseBody);
                        result = ConnectionCallback.ConnResult.SUCCESS;
                    }


                }catch (Exception e){

                }
                callBack.connectionDidFinished(result, errorMsg, null);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String errorMsg = getRetrofitErrorMsg(t);
                callBack.connectionDidFinished(result, errorMsg, null);
            }
        });

    }



    public static String getRetrofitErrorMsg(Throwable t){
        String msg = "";
        try{
            LogHelper.error("[Error][getMessage] = " + t.getMessage());
            msg = t.getLocalizedMessage();
        }catch (Exception e){
            msg = "Error";
            e.printStackTrace();
        }

        return msg;
    }


}
