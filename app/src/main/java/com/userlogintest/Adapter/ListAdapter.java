package com.userlogintest.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.userlogintest.Interface.ItemCallback;
import com.userlogintest.Model.ItemModel;
import com.userlogintest.R;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.VH> {

    List<ItemModel> list = new ArrayList<>();
    ItemCallback callback;

    public ListAdapter(List<ItemModel> _list, ItemCallback _callback){
        this.callback = _callback;
        this.list = _list;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new VH(itemView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        ItemModel mItem = list.get(position);
        holder.itemName.setText(mItem.getName());
        holder.itemId.setText(mItem.getId());

        holder.itemView.setTag(mItem);
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(v.getTag()!=null) {
                    callback.onItemLongclick((ItemModel) v.getTag());
                }
                return false;
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getTag()!=null) {
                    callback.onItemClick((ItemModel) v.getTag());
                }
            }
        });
    }

    public void addItem(ItemModel _mItem){
        if(_mItem!=null){
            this.list.add(_mItem);
        }
    }

    public class VH extends RecyclerView.ViewHolder{

        TextView itemName;
        TextView itemId;

        public VH(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_text);
            itemId = itemView.findViewById(R.id.item_id);

        }
    }
}
