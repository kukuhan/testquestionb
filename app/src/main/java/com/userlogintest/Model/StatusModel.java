package com.userlogintest.Model;

public class StatusModel {

    public static final int SUCCESS_CODE = 200;

    int code;
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
