package com.userlogintest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.userlogintest.Adapter.ListAdapter;
import com.userlogintest.Connection.ConnectionHolder;
import com.userlogintest.Interface.ConnectionCallback;
import com.userlogintest.Interface.ItemCallback;
import com.userlogintest.Model.ItemModel;
import com.userlogintest.Utils.LogHelper;
import com.userlogintest.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class  MainActivity extends AppCompatActivity {

    RecyclerView rv;
    Button logoutBtn;

    private static int INCREMENT_NUM = 10;
    int currentNumber = 0;

    boolean isLoading = false;
    Dialog dialog;

    Toast t;
    List<ItemModel> list = new ArrayList<>();

    ListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv = findViewById(R.id.recycler_view);
        logoutBtn = findViewById(R.id.logout);

        getListing();
        setupButton();
    }

    private void setupButton(){
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogoutConfirmation();
            }
        });
    }

    private void getListing() {
        showDialog(this);
        ConnectionHolder.getListing(this, new ConnectionCallback.GetObjectCallBack() {
            @Override
            public void connectionDidFinished(ConnectionCallback.ConnResult result, String responseMessage, Object object) {
                hideDialog();
                if (object != null && result == ConnectionCallback.ConnResult.SUCCESS) {
                    list = (List<ItemModel>) object;
                    LogHelper.debug("size " + list.size());
                    setupList();
                }
            }
        });
    }

    private void showEditDialog(final ItemModel _mItem) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_editinfo, null);
        dialogBuilder.setView(dialogView);
        final EditText editName = dialogView.findViewById(R.id.edit_name);
        final EditText editDistance = dialogView.findViewById(R.id.edit_distance);
        editName.setText(_mItem.getName());
        editDistance.setText(_mItem.getDistance());

        dialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (editDistance == null
                        || editName == null) {
                    return;
                }

                ItemModel tempItem = new ItemModel();
                tempItem.setId(_mItem.getId());
                tempItem.setName(editName.getText().toString());
                tempItem.setDistance(editDistance.getText().toString());
                updateInfo(tempItem);
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void updateInfo(ItemModel mItem) {
        LogHelper.debug("Token " + SharedPrefManager.getInstance(this).getString(SharedPrefManager.TAG_USER_TOKEN));
        ConnectionHolder.updateItem(this, mItem, new ConnectionCallback.GetObjectCallBack() {
            @Override
            public void connectionDidFinished(ConnectionCallback.ConnResult result, String responseMessage, Object object) {
                showMessage("", responseMessage);
            }
        });
    }

    private void setupList() {

        List<ItemModel> tempList = new ArrayList<>();
        if (list.size() > INCREMENT_NUM) {
            int i;
            for (i = 0; i < INCREMENT_NUM; i++) {
                tempList.add(list.get(i));
            }
            currentNumber = INCREMENT_NUM;
        } else {
            tempList.addAll(list);
            currentNumber = list.size();
        }
        mAdapter = new ListAdapter(tempList, new ItemCallback() {
            @Override
            public void onItemLongclick(ItemModel mItem) {
                showEditDialog(mItem);
            }

            @Override
            public void onItemClick(ItemModel mItem) {

                if (t != null) {
                    t.cancel();
                }
                StringBuilder builder = new StringBuilder();
                builder.append("Name : ");
                builder.append(mItem.getName());
                builder.append(", Distance : ");
                builder.append(mItem.getDistance());
                t = Toast.makeText(MainActivity.this, builder.toString(), Toast.LENGTH_SHORT);
                t.show();
            }
        });
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                mLayoutManager.getOrientation());
        rv.addItemDecoration(dividerItemDecoration);
        rv.setAdapter(mAdapter);
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                if (isLoading) {
                    return;
                }
                showDialog(MainActivity.this);
                int totalItemCount = mLayoutManager.getItemCount();
                int lastVisiblePosition = mLayoutManager.findLastVisibleItemPosition();
                if (lastVisiblePosition == (totalItemCount - 1)) {
                    isLoading = true;
                    if (mAdapter.getItemCount() < list.size()) {
                        int nextMaxNumber;
                        if ((currentNumber + INCREMENT_NUM) < list.size()) {
                            nextMaxNumber = currentNumber + INCREMENT_NUM;
                        } else {
                            nextMaxNumber = list.size();
                        }
                        LogHelper.debug("[nextMaxNumber] " + nextMaxNumber);
                        for (int x = currentNumber; x < nextMaxNumber; x++) {
                            mAdapter.addItem(list.get(x));
                        }
                        rv.post(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                        currentNumber = nextMaxNumber;
                    }
                    isLoading = false;

                } else {
                    isLoading = false;
                }

                hideDialog();
            }
        });

    }

    public void showLogoutConfirmation(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.logout);
        builder.setMessage(R.string.logout_confirmation);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPrefManager.getInstance(MainActivity.this).putString(SharedPrefManager.TAG_USER_TOKEN, "");
                SharedPrefManager.getInstance(MainActivity.this).putString(SharedPrefManager.TAG_USER_ID, "");
                SharedPrefManager.getInstance(MainActivity.this).putBoolean(SharedPrefManager.TAG_IS_LOGIN, false);
                dialog.dismiss();
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                MainActivity.this.finish();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    public void showMessage(String title, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    public void showDialog(Context c) {

        dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //...set cancelable false so that it's never get hidden
        dialog.setCancelable(false);
        //...that's the layout i told you will inflate later
        dialog.setContentView(R.layout.custom_loading_layout);

        //...finaly show it
        dialog.show();
    }

    //..also create a method which will hide the dialog when some work is done
    public void hideDialog() {
        if (dialog != null){
            dialog.dismiss();
        }

    }

}
