package com.userlogintest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.userlogintest.Connection.ConnectionHolder;
import com.userlogintest.Interface.ConnectionCallback;
import com.userlogintest.Utils.LogHelper;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Button loginBtn;

    EditText email;
    EditText password;

    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginBtn = findViewById(R.id.login_btn);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        email.setText("admin@advisoryapps.com");
        password.setText("advisoryapps123");

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

    }

    public void loginUser(){

        String inEmail = "";
        String inPassword ="";

        if(email!=null && email.getText()!=null){
            inEmail = email.getText().toString();
        }
        if(password!=null && password.getText()!=null){
            inPassword = password.getText().toString();
        }

        Map<String, String> map = new HashMap<>();
        map.put("email", inEmail);
        map.put("password", inPassword);
        LogHelper.debug("[map] = " + map.toString());
        showDialog(LoginActivity.this);
        ConnectionHolder.signInUser(this, map, new ConnectionCallback.GetObjectCallBack() {
            @Override
            public void connectionDidFinished(ConnectionCallback.ConnResult result, String responseMessage, Object object) {
                Toast.makeText(LoginActivity.this, responseMessage, Toast.LENGTH_SHORT).show();

                if(result== ConnectionCallback.ConnResult.SUCCESS) {
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    LoginActivity.this.finish();
                }
                hideDialog();

            }
        });
    }

    public void showMessage(String title, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    public void showDialog(Context c) {

        dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //...set cancelable false so that it's never get hidden
        dialog.setCancelable(false);
        //...that's the layout i told you will inflate later
        dialog.setContentView(R.layout.custom_loading_layout);

        //...finaly show it
        dialog.show();
    }

    //..also create a method which will hide the dialog when some work is done
    public void hideDialog() {
        if (dialog != null){
            dialog.dismiss();
        }

    }
}
