package com.userlogintest.Utils;

public class ApiConfigs {

    public static final String API_PRODUCTION_URL = "https://interview.advisoryapps.com/index.php/";
    public static final String API_LOGIN = "login";
    public static final String API_LISTING = "listing";
    public static final String API_UPDATE = API_LISTING+"/update";

}
