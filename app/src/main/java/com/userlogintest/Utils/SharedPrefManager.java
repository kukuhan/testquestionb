package com.userlogintest.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kuku on 02/11/2018.
 */

public class SharedPrefManager {
    private static final String PREFS_NAME = "UserLoginTest";
    private static SharedPrefManager mSharePreferencesManager;
    private SharedPreferences mSharePreferences;
    private SharedPreferences.Editor mEditor;

    public static final String TAG_IS_LOGIN = "isLogin";

    public static final String TAG_USER_ID = "user_id";
    public static final String TAG_USER_TOKEN = "token";

    public static SharedPrefManager getInstance(Context context) {
        if (mSharePreferencesManager == null) {
            mSharePreferencesManager = new SharedPrefManager(context.getApplicationContext());
        }
        return mSharePreferencesManager;
    }

    private SharedPrefManager(Context context){
        mSharePreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }


    public void putString(String key, String value){
        SharedPreferences.Editor editor = mSharePreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void putInt(String key, int value){
        SharedPreferences.Editor editor = mSharePreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void putLong(String key, long value){
        SharedPreferences.Editor editor = mSharePreferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public void putBoolean(String key, Boolean value){
        SharedPreferences.Editor editor = mSharePreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    public String getString(String key){
        return mSharePreferences.getString(key, "");
    }

    public int getInt(String key){
        return mSharePreferences.getInt(key, 0);
    }

    public long getLong(String key){
        return mSharePreferences.getLong(key, 0);
    }

    public long getLong(String key, long defaultLong){
        return mSharePreferences.getLong(key, defaultLong);
    }

    public boolean getBoolean(String key, boolean defaultLong){
        return mSharePreferences.getBoolean(key, defaultLong);
    }

    public void delete(String key){
        SharedPreferences.Editor editor = mSharePreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public void clearAll(){
        SharedPreferences.Editor editor = mSharePreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void initialBulkUpdate(){
        mEditor = mSharePreferences.edit();
    }

    public void bulkPutString(String key, String value){
        mEditor.putString(key, value);
    }

    public void bulkPutInt(String key, int value){
        mEditor.putInt(key, value);
    }

    public void bulkPutLong(String key, long value){
        mEditor.putLong(key, value);
    }

    public void bulkCommit(){
        mEditor.commit();
        mEditor = null;
    }

    ////////////////////////////////////////////////////////////////////

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static void putString(Context context, String key, String value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key , value);
        editor.commit();
    }

    public static String getString(Context context, String key) {
        return getSharedPreferences(context).getString(key , null);
    }

}