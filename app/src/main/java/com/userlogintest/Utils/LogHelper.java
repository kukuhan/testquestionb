package com.userlogintest.Utils;

/**
 * Created by kuku on 29/08/2017.
 */

import android.content.Context;
import android.util.Log;

public class LogHelper {

    public static final String TAG = "UserLoginTest";

    public static final int LEVEL_DEBUG = 0;
    public static final int LEVEL_INFO = 1;
    public static final int LEVEL_ERROR = 2;

    public static final int MAX_LENGTH = 1000;

    private static int logLevel = LEVEL_DEBUG;

    public static void setLogLevel(int level) {
        if (level > LEVEL_DEBUG || level < LEVEL_DEBUG) {
            logLevel = level;
        } else {
            logLevel = 0;
        }
    }

    public static void debug( String message) {
        if (logLevel <= LEVEL_DEBUG) {

            for(int i = 0; i <= message.length() / MAX_LENGTH; i++) {
                int start = i * MAX_LENGTH;
                int end = (i+1) * MAX_LENGTH;
                end = end > message.length() ? message.length() : end;
                Log.d(TAG, message.substring(start, end));
            }
        }
    }

    public static void log(Context _context, String message) {
        if (logLevel <= LEVEL_DEBUG) {
            Log.d(TAG, "["+_context.getClass().getSimpleName()+"] " + message);
        }
    }

    public static void info(String message) {
        if (logLevel <= LEVEL_INFO) {
            Log.i(TAG, "[INFO]  " + message);
        }
    }

    public static void error(String message) {
        if (logLevel <= LEVEL_ERROR) {
            Log.e(TAG, "[ERROR] " + message);
        }
    }

}