package com.userlogintest.Utils;

import com.userlogintest.Model.ItemModel;
import com.userlogintest.Model.StatusModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSonHelper {

    public static final String TAG_STATUS_CODE = "code";
    public static final String TAG_STATUS_MESSAGE = "message";

    public static final String MAINTAINANCE_ANDROID = "maintenance";

    public static boolean getObjBoolean(JSONObject json, String tag, boolean defaultValue) {
        boolean result = defaultValue;
        if (json == null || json.equals("")) {
            return result;
        }
        try {

            if (json.has(tag)) {
                result = json.getBoolean(tag);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
        return result;
    }

    public static String getObjString(JSONObject json, String tag) {
        return getObjString(json, tag, "");
    }

    public static String getObjString(JSONObject json, String tag, String defaulText) {
        String result = "";
        if (json == null || json.equals("")) {
            return result;
        }
        try {

            if (json.has(tag)) {
                result = json.getString(tag);
                if (result == null || result.length() < 0) {
                    return "";
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result = defaulText;
        }
        return result;
    }

    public static int getObjInt(JSONObject json, String tag) {
        int result = 0;
        if (json == null) {
            return result;
        }
        try {

            result = json.getInt(tag);
            if (result == 0) {
                return 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getObjInt(JSONObject json, String tag, int defaultValue) {
        int result = 0;
        if (json == null) {
            return result;
        }
        try {

            result = json.getInt(tag);
            if (result == 0) {
                return 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result = defaultValue;
        }
        return result;
    }

    public static double getObjDouble(JSONObject json, String tag) {
        double result = 0;
        if (json == null) {
            return result;
        }
        try {

            result = json.getDouble(tag);
            if (result == 0) {
                return 0;
            }
        } catch (JSONException e) {
            LogHelper.error(e.getLocalizedMessage());
        }
        return result;
    }

    public static Map<String, String> parseUserLogin(String response){
        Map<String, String> map = new HashMap<>();
        try{
            JSONObject json = new JSONObject(response);
            map.put("id", getObjString(json, "id"));
            map.put("token", getObjString(json, "token"));
        }catch (Exception e){
            e.printStackTrace();
        }

        return map;
    }

    public static StatusModel parseStatus(String response){
        StatusModel status = new StatusModel();
        try{
            JSONObject json = new JSONObject(response);
            JSONObject jStatus = json.getJSONObject("status");
            status.setCode(getObjInt(jStatus, JSonHelper.TAG_STATUS_CODE));
            status.setMessage(getObjString(jStatus, JSonHelper.TAG_STATUS_MESSAGE));
        }catch (Exception e){
            e.printStackTrace();
        }

        return status;
    }

    public static List<ItemModel> parseItem(String response){
        List<ItemModel> list = new ArrayList<>();
        try{
            JSONObject json = new JSONObject(response);
            JSONArray jArr = json.getJSONArray("listing");
            for(int i = 0; i < jArr.length(); i++){
                JSONObject jChild = jArr.getJSONObject(i);
                ItemModel mItem = new ItemModel();
                mItem.setId(getObjString(jChild, "id"));
                mItem.setDistance(getObjString(jChild, "distance"));
                mItem.setName(getObjString(jChild, "list_name"));
                list.add(mItem);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return list;
    }

}
