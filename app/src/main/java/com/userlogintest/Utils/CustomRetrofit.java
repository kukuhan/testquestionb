package com.userlogintest.Utils;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public class CustomRetrofit {

    private static OkHttpClient httpClient = new OkHttpClient();

    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(ApiConfigs.API_PRODUCTION_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.client(httpClient).build();

    private static CustomRetrofit instance = new CustomRetrofit();

    private static ApiService connService;

    private CustomRetrofit() {

        Retrofit retrofit = createAdapter().build();
        connService = retrofit.create(ApiService.class);
    }

    public static CustomRetrofit getInstance(){
        if(instance==null){
            instance = new CustomRetrofit();
        }
        return instance;
    }

    private Retrofit.Builder createAdapter() {
        initHttpClient();
        return new Retrofit.Builder()
                .baseUrl(ApiConfigs.API_PRODUCTION_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create());
    }

    private void initHttpClient(){
        httpClient =  new OkHttpClient.Builder().connectTimeout(500, TimeUnit.SECONDS)
                .writeTimeout(500, TimeUnit.SECONDS)
                .readTimeout(500, TimeUnit.SECONDS)
                .build();
    }

    public interface ApiService {
        @FormUrlEncoded
        @POST(ApiConfigs.API_LOGIN)
        Call<ResponseBody> login(
                @Field("email") String email,
                @Field("password") String password);

        @GET(ApiConfigs.API_LISTING)
        Call<ResponseBody> listing(@QueryMap Map<String, String> map);

        @FormUrlEncoded
        @POST(ApiConfigs.API_UPDATE)
        Call<ResponseBody> update(@FieldMap Map<String, String> map);
    }

    public Call<ResponseBody> login(Map<String, String> map){
        return connService.login(map.get("email"), map.get("password"));
    }

    public Call<ResponseBody> listing(Map<String, String> map){
        return connService.listing(map);
    }

    public Call<ResponseBody> update(Map<String, String> map){
        return connService.update(map);
    }
}
